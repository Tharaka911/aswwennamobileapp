import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:login2/UI/Commenclass/transportreq.dart';
import 'package:login2/UI/Transpoter/confirmedtranceport.dart';
import 'package:login2/UI/Transpoter/transportreqtransporter.dart';
import 'package:login2/main.dart';
import 'package:login2/userclass.dart';

class Transpoterhome extends StatefulWidget {
  Usr logeduser;

  Transpoterhome({this.logeduser});

  @override
  _TranspoterhomeState createState() => _TranspoterhomeState();
}

class _TranspoterhomeState extends State<Transpoterhome> {
  
  String point = 'Home';
  String header = 'Home';

  Widget nav() {
    if (point == 'AllItem') {
      //return Buyerdefaultpage(
       // logeduser: widget.logeduser,
     // );
    } else if (point == 'Sentreq') {
      // return Sentrequest(
      //   logeduser: widget.logeduser,
      // );
    } else if (point == 'Home') {
       return Transportreqtranporter(id: widget.logeduser.id.toString());
    }
    else if(point == 'confirmed'){
       return Confirmedtranceport(id: widget.logeduser.id.toString(),);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(header),
        backgroundColor: Colors.orange,
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //header//
            new UserAccountsDrawerHeader(
              accountName: Text(widget.logeduser.fullname),
              accountEmail: Text(widget.logeduser.mobilenumber),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration: new BoxDecoration(color: Colors.orange),
            ),
            //body//
            InkWell(
              onTap: () {
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => FarmerProfile()));
                Navigator.pop(context);

              },
              child: ListTile(
                title: Text('My profile'),
                leading: Icon(Icons.person,color: Colors.orangeAccent),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                 point='Home'; 
                });
                Navigator.pop(context);
              },
              child: ListTile(
                title: Text('Explore'),
                leading: Icon(Icons.explore,color: Colors.orangeAccent,),
              ),
            ),
            InkWell(
              onTap: () {
                // initState();
                // setState(() {
                //   isloading = false;
                // });
                //loadrequest();
                 Navigator.pop(context);

              },
              child: ListTile(
                title: Text('Statistics'),
                leading: Icon(Icons.table_chart,color: Colors.orangeAccent),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                 point='confirmed'; 
                 
                });
                Navigator.pop(context);

              },
              child: ListTile(
                title: Text('Confirmed order'),
                leading: Icon(Icons.shopping_basket,color: Colors.orangeAccent),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacement((context),
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
              child: ListTile(
                title: Text('Login Out'),
                leading: Icon(Icons.lock_open,color: Colors.orangeAccent),
              ),
            ),
          ],
        ),
      ),
      body: nav()
    );
  }
}
