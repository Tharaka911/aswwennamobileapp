import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:login2/UI/Farmer/Farmer.dart';
import 'package:login2/productclass.dart' as prodct;
import 'package:http/http.dart' as http;
import 'package:login2/Method/buyermethod.dart';

class AddPurchase extends StatefulWidget {
  int fid;

  AddPurchase({this.fid});

  @override
  AddItemstate createState() {
    return AddItemstate();
  }
}

class AddItemstate extends State<AddPurchase> {
  final _formadditem = GlobalKey<FormState>();
  PurchaseItem product = new PurchaseItem();
  int selectedRadio;
  int _categoryName = 2;
  bool isloading = false;
  String networkstate = '';
  prodct.Product product2;

  //get http => null;

  // Widget build(context) {
//   setState(() => this.context = context);

// }

  // setSelectedRadio(int val) {
  //   setState(() {
  //     selectedRadio = val;
  //     _categoryName = val;
  //   });
  // }

  Future _purItem() async {
    Map arr = {
      'category': product.category,
      'type': product.type,
      'quantity': product.quantity,
      'exp_date': product.exp_date,
      //'price': product.price,
      'location': product.location
    };
    var js = jsonEncode(arr);
    print(js);
    var url = 'http://34.87.38.40/purchaseItem/farmer/' + widget.fid.toString();

    final response = await http
        .post(Uri.encodeFull(url),
            headers: {"Content-Type": "application/json"}, body: js)
        .then((response) {
      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        ItemAddedpopup(context);
        _formadditem.currentState.reset();
      } else if (response.statusCode >= 500) {
        ServerErrorPopup(context);
      } else if (response.statusCode >= 400 && response.statusCode < 500) {
      } else if (response.body == '[]') {
        NoItempopup(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.8,
        backgroundColor: Colors.lightGreen,
        title: Text('Add Item'),
        actions: <Widget>[
          new IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FarmerInfo()));
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Center(
            child: Container(
              width: 320,
              height: 640,
              child: Form(
                key: _formadditem,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Material(
                        //elevation: 10.0,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        child: Image.asset(
                          'images/tomato.jpg',
                          width: 100,
                          height: 100,
                        ),
                      ),

                      Column(
                        children: <Widget>[
                          Container(
                            //5height: 150,
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text('Catogory:',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0)),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Radio(
                                      value: 0,
                                      groupValue: _categoryName,
                                      activeColor: Colors.green,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category = 'vegitable';
                                        });
                                      },
                                    ),
                                    Text('vegitable'),
                                    Radio(
                                      value: 1,
                                      groupValue: _categoryName,
                                      activeColor: Colors.blue,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category = 'fruit';
                                        });
                                      },
                                    ),
                                    Text(
                                      'Fruits',
                                    ),
                                    Radio(
                                      value: 2,
                                      groupValue: _categoryName,
                                      activeColor: Colors.deepOrangeAccent,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category = 'spicies';
                                        });
                                      },
                                    ),
                                    Text('spicies'),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Radio(
                                      value: 4,
                                      groupValue: _categoryName,
                                      activeColor: Colors.green,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category = 'coconut';
                                        });
                                      },
                                    ),
                                    Text('Coconut'),
                                    Radio(
                                      value: 5,
                                      groupValue: _categoryName,
                                      activeColor: Colors.blue,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category='tea';
                                        });
                                      },
                                    ),
                                    Text('Tea'),
                                    Radio(
                                      value: 6,
                                      groupValue: _categoryName,
                                      activeColor: Colors.deepOrangeAccent,
                                      onChanged: (Object value) {
                                        setState(() {
                                          _categoryName = value;
                                          product.category = 'other';
                                        });
                                      },
                                    ),
                                    Text('Other'),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // TextFormField(
                      //   decoration: InputDecoration(
                      //       //border: InputBorder.none,
                      //       border: OutlineInputBorder(
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(10.0)),
                      //       ),
                      //       hintText: 'category',
                      //       fillColor: Colors.grey[100],
                      //       filled: true,
                      //       prefixIcon: Icon(
                      //         Icons.format_size,
                      //         color: Colors.lightGreen,
                      //       )),
                      //   style: TextStyle(
                      //     fontSize: 12.0,
                      //     color: Colors.black,
                      //   ),
                      //   validator: (value) {
                      //     if (value.isEmpty)
                      //       return "This can not be empty";
                      //     else if (value.length > 15)
                      //       return "Type can not contain more than 15 characters";
                      //     else
                      //       return null;
                      //   },
                      //   onSaved: (value) {
                      //     product.category = value;
                      //     print("category:" + product.category);
                      //   },
                      // ),
                      TextFormField(
                        decoration: InputDecoration(
                            //border: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            hintText: 'Type',
                            fillColor: Colors.grey[100],
                            filled: true,
                            prefixIcon: Icon(
                              Icons.format_size,
                              color: Colors.lightGreen,
                            )),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value.isEmpty)
                            return "This can not be empty";
                          else if (value.length > 15)
                            return "Type can not contain more than 15 characters";
                          else
                            return null;
                        },
                        onSaved: (value) {
                          product.type = value;
                          print("Type:" + product.type);
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            //border: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            hintText: 'Quantity',
                            fillColor: Colors.grey[100],
                            filled: true,
                            prefixIcon: Icon(
                              Icons.format_size,
                              color: Colors.lightGreen,
                            )),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value.isEmpty)
                            return "This can not be empty";
                          // else if (value.length > 15)
                          //   return "quantity can not contain more than 15 characters";
                          else
                            return null;
                        },
                        onSaved: (value) {
                          product.quantity = value;
                          print("Quantity:" + product.quantity);
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            //border: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            hintText: 'Exp date',
                            fillColor: Colors.grey[100],
                            filled: true,
                            prefixIcon: Icon(
                              Icons.date_range,
                              color: Colors.lightGreen,
                            )),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value.isEmpty)
                            return "This can not be empty";
                          // else if (value.length > 15)
                          //   return "Name can not contain more than 15 characters";
                          else
                            return null;
                        },
                        onSaved: (value) {
                          product.exp_date = value;
                          print("exp date:" + product.exp_date);
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            //border: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            hintText: 'price',
                            fillColor: Colors.grey[100],
                            filled: true,
                            prefixIcon: Icon(
                              Icons.monetization_on,
                              color: Colors.lightGreen,
                            )),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value.isEmpty)
                            return "This can not be empty";
                          // else if (value.length > 15)
                          //   return "Name can not contain more than 15 characters";
                          else
                            return null;
                        },
                        onSaved: (value) {
                          // product.price = value;
                          // print("price:" + product.price);
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            //border: InputBorder.none,
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            hintText: 'Location',
                            fillColor: Colors.grey[100],
                            filled: true,
                            prefixIcon: Icon(
                              Icons.location_on,
                              color: Colors.lightGreen,
                            )),
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value.isEmpty)
                            return "This can not be empty";
                          // else if (value.length > 15)
                          //   return "Name can not contain more than 15 characters";
                          else
                            return null;
                        },
                        onSaved: (value) {
                          product.location = value;
                          print("location:" + product.location);
                        },
                      ),
                      // Container(
                      //   width: 150,
                      //   height: 40,
                      //   child: RaisedButton(onPressed: (){
                      //     Navigator.push(context,
                      //     MaterialPageRoute(builder: (context)=> LocationMap()));
                      //     },
                      //   color: Colors.white24,textColor: Colors.black,
                      //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      //   child: Text('Location',
                      //                 style: TextStyle(fontSize: 16.0),
                      //                 ),
                      //           // Image.asset('images/google_map.png',height: 50,width: 60,)
                      //   ),
                      // ),
                      Container(
                        width: 300,
                        height: 45,
                        child: RaisedButton(
                          onPressed: () {
                            //_purItem();
                            if (_formadditem.currentState.validate()) {
                              _formadditem.currentState.save();
                              _purItem();
                            }
                          },
                          color: Colors.deepOrange,
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0))),
                          child: Text(
                            'Submit',
                            style: TextStyle(fontSize: 20.0),
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PurchaseItem {
  String category;
  String type;
  String quantity;
  //String price;
  String location;
  String exp_date;

  get Product => null;
}
