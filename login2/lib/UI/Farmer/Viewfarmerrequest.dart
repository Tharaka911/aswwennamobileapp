import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:login2/Method/buyermethod.dart';
import 'package:login2/UI/Buyer/Sentrequest.dart';
import 'package:login2/UI/Commenclass/Products.dart';
import 'package:login2/UI/Farmer/myitemlist.dart';
import 'package:http/http.dart' as http;
import 'package:login2/userclass.dart';

class Viewrequest extends StatefulWidget {
  product myitem;
  String purchase_id;
  String farmer_id;

  Viewrequest({this.purchase_id, this.farmer_id, this.myitem});

  @override
  _ViewrequestState createState() => _ViewrequestState(
      purchase_id: purchase_id, farmer_id: farmer_id, myitem: myitem);
}

class _ViewrequestState extends State<Viewrequest> {
  String purchase_id;
  String farmer_id;
  product myitem;
  List<Usr> buyerlist;
  bool isloading = true;
  _ViewrequestState({this.purchase_id, this.farmer_id, this.myitem});

  reqestedbuyer() async {
    String url = 'http://34.87.38.40/purchaseItem/' +
        purchase_id.toString() +
        '/requestedBuyers/' +
        farmer_id.toString();

    print(url);
    print(widget.purchase_id);

    await http.get(url).then((response) {
      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {
        String content = response.body;
        List prdct = json.decode(content);
        if (content == '[]') {
          NoItempopup(this.context);
        } else {
          setState(() {
            buyerlist = prdct.map((json) => Usr.fromJson(json)).toList();
            isloading = false;
          });
          print(buyerlist[0].fullname);
        }
      } else if (response.statusCode >= 400 && response.statusCode < 500) {
        print('error');
      } else if (response.statusCode >= 500) {
        ServerErrorPopup(this.context);
      }
    });
  }

  confirmbuyer(String buyrid) async{
    String url = 'http://34.87.38.40/purchaseItem/'+myitem.purchase_item_id.toString()+'/select_buyer/'+buyrid;

    await http.post(url,body: {"type":"type"}).then((response){
      print(response.statusCode);
      print(response.body);
      if(response.statusCode==200){
        SentRequestPopup(context);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    reqestedbuyer();
    //print(myitem.district);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[70],
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.green.withOpacity(0.75),
        leading: Container(
          height: 20,
          width: 20,
          //color: Colors.amber,
          // decoration: BoxDecoration(
          //     shape: BoxShape.circle, color: Colors.grey.withOpacity(0.1)),
          child: IconButton(
            iconSize: 20,
            //padding: EdgeInsets.all(1),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ),
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      //color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  margin: EdgeInsets.only(bottom: 5),
                  //color: Colors.grey,
                  child: ListTile(
                    //contentPadding: EdgeInsets.all(10.0),
                    leading: Container(
                      height: 100,
                      width: 60,
                      // decoration: BoxDecoration(
                      //   shape: BoxShape.circle,
                      //   color: Colors.grey[100],
                      //   image: DecorationImage(
                      //       image: ExactAssetImage('images/avacado.jpg'),
                      //       fit: BoxFit.fill),
                      // )
                      child: Image.asset(
                        'images/avacado.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    title: Padding(
                      padding: EdgeInsets.only(top: 0.0, bottom: 10),
                      //color: Colors.black,
                      // alignment: Alignment.topLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            //Text(
                            myitem.type.toUpperCase() +
                                (' (') +
                                myitem.variety.toString()+(')'),
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          Text(myitem.unit_price.toString()+' Rs / kg',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold)),
                          Text(myitem.quantity.toString()+' kg',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold))
                        ],
                      ),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          myitem.district+','+myitem.city,
                          style: TextStyle(
                              fontWeight: FontWeight.w300, fontSize: 10),
                        ),
                      ],
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(myitem.date,
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold)),
                        // Text(
                        //   '( 2 more Days to expire )',
                        //   style: TextStyle(
                        //       fontSize: 8,
                        //       fontWeight: FontWeight.bold,
                        //       color: Colors.red),
                        // )
                        timediffrence(myitem.exp_date)
                      ],
                    ),
                    // onTap: () {
                    //   Navigator.push(
                    //       context,
                    //       MaterialPageRoute(
                    //           builder: (context) => Viewrequest()));
                    // },
                  ),
                ),
              ],
            ),
          ),
          // Container(
          //   color: Colors.grey,
          //   height: 1,
          //   width: MediaQuery.of(context).size.width,
          //   margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
          // ),
          Container(
            padding: EdgeInsets.only(left: 10),
            color: Colors.white,
            //height: 1,
            width: MediaQuery.of(context).size.width,
            //padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
            child: Text(
              'Requests',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
          ),
          Container(
            color: Colors.grey,
            height: 1,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
          ),
          Expanded(
              flex: 1,
              child: RefreshIndicator(
                onRefresh: () async {
                  reqestedbuyer();
                  await Future.delayed(Duration(seconds: 2));
                },
                color: Colors.blue,
                child: Container(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: isloading
                      ? ListView(
                          children: <Widget>[
                            Center(
                              child: Text('No request yet'),
                            )
                          ],
                        )
                      : ListView.builder(
                          itemCount: buyerlist.length,
                          itemBuilder: (context, index) {
                            return Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.zero),
                              color: Colors.white,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            EdgeInsets.fromLTRB(10, 5, 5, 5),
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.lightBlue,
                                            image: DecorationImage(
                                                image: ExactAssetImage(
                                                    'images/farmer.png'),
                                                fit: BoxFit.cover)),
                                        //   child: Icon(
                                        //     Icons.ac_unit,
                                        //     size: 40,
                                        //   ),
                                      ),
                                      Expanded(
                                          flex: 2,
                                          child: Container(
                                              margin: EdgeInsets.only(left: 20),
                                              child: Text(
                                                buyerlist[index].fullname,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 15),
                                              ))),
                                      Expanded(
                                          flex: 1,
                                          child: Container(
                                              padding:
                                                  EdgeInsets.only(right: 20),
                                              alignment: Alignment.centerRight,
                                              child: Text(DateTime.now().toString())))
                                    ],
                                  ),
                                  Container(
                                    //margin: EdgeInsets.only(left: 10, right: 10),
                                    height: 1,
                                    color: Colors.grey,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: RatingBarIndicator(
                                              itemSize: 15,
                                              itemCount: 5,
                                              rating: buyerlist[index].starcount,
                                              itemBuilder: (context, int) =>
                                                  Icon(
                                                Icons.star,
                                                color: Colors.red,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              '(180)',
                                              style: TextStyle(fontSize: 10),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                          child: FlatButton(
                                        child: Text('Confirm'),
                                        onPressed: () {
                                          confirmbuyer(buyerlist[index].id.toString());
                                        },
                                      ))
                                    ],
                                  )
                                  //Text('data'),
                                  //Text('data'),
                                ],
                              ),
                            );
                          },
                        ),
                ),
              ))
        ],
      ),
    );
  }
}
