import 'package:flutter/material.dart';

class Notificate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Notificatiionstate();
  }
}

class _Notificatiionstate extends State<Notificate> {
  bool isloading = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Notification"),
        backgroundColor: Colors.lightGreen,
        actions: <Widget>[
          new IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        height: double.maxFinite,
        child: isloading
            ? CircularProgressIndicator()
            : Center(
                child: IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    setState(() {
                     isloading=!isloading; 
                    });
                  },
                ),
              ),
      ),
    );
  }
}
