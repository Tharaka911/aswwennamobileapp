import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:login2/UI/Commenclass/filter.dart';
import 'package:login2/UI/Commenclass/listtile.dart';
import 'package:login2/UI/Farmer/FarmerProfile.dart';
import 'package:login2/UI/Farmer/additem/selectType.dart';
import 'package:login2/UI/Farmer/additem/selectcategory.dart';
import 'package:login2/UI/Farmer/confirmedorderfarmer.dart';
import 'package:login2/UI/Farmer/myitemlist.dart';
import 'package:login2/UI/Farmer/notification.dart';
import 'package:login2/detaileduser.dart';
import 'package:login2/main.dart';
import 'package:http/http.dart' as http;
import 'package:login2/userclass.dart' as user;
import 'AddItem.dart';
import 'HorizentalList.dart';
import 'package:login2/userclass.dart';
import 'package:login2/UI/Commenclass/notificationstruct.dart';
import 'package:login2/UI/Commenclass/Products.dart';
import 'package:login2/Method/buyermethod.dart';
import 'package:login2/UI/Farmer/AddedItemList.dart';
import 'package:login2/UI/Commenclass/AboutAs.dart';

class FarmerInfo extends StatefulWidget {
  user.Usr logeduser;
  Detaileduser userdetails;
  String url = 'http://34.87.38.40/purchaseItem/viewAll';

  FarmerInfo({this.logeduser, this.url, this.userdetails});

  @override
  createState() {
    return FarmerInfostate();
  }
}

class FarmerInfostate extends State<FarmerInfo> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // print(widget.userdetails.user.farmer_id.toString());
  }

  var page = 'myitemlist';

  Widget _Drawernavigaor() {
    if (page == 'myitemlist') {
      //return ViewallItem();
      return Myitem(
        id: widget.logeduser.id,
      );
    } else if (page == 'viewall') {
      return ViewallItem();
    } else if (page == 'myconfirmitem')
      return Myconfirmitem(id: widget.logeduser.id,);
    else
      return Notificate();
  }

  @override
  Widget build(BuildContext context) {
    // setState(() {
    //   _context = context;
    // });

    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      extendBody: true,
      appBar: new AppBar(
        backgroundColor: Colors.green.withOpacity(0.75),
        title: Text('Farmer'),
        actions: <Widget>[
          // new IconButton(
          //   icon: Icon(
          //     Icons.notifications,
          //     color: Colors.white,
          //   ),
          //   onPressed: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => Notificate()));
          //   },
          // ),
          new IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //header//
            new UserAccountsDrawerHeader(
              accountName: Text(widget.logeduser.fullname),
              accountEmail: Text(widget.logeduser.mobilenumber),
              currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Image.asset(
                    'images/farmer.png',
                    fit: BoxFit.fitHeight,
                    height: 60,
                  ) //Icon(
                  //   Icons.person,
                  //   color: Colors.white,
                  // ),
                  ),
              decoration: new BoxDecoration(color: Colors.lightGreen),
            ),
            //body//
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FarmerProfile()));
              },
              child: ListTile(
                title: Text('My profile'),
                leading: Icon(
                  Icons.person,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  page = 'myitemlist';
                });
              },
              child: ListTile(
                title: Text('My Added List'),
                leading: Icon(
                  Icons.playlist_add_check,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  page = 'viewall';
                });
              },
              child: ListTile(
                title: Text('Explore'),
                leading: Icon(
                  Icons.search,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                initState();
              },
              child: ListTile(
                title: Text('Statistics'),
                leading: Icon(
                  Icons.table_chart,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                setState(() {
                  page = 'myconfirmitem';
                });
                //Navigator.push(context, MaterialPageRoute(builder: (context)=>Myconfirmitem()));
              },
              child: ListTile(
                title: Text('Confirmed order'),
                leading: Icon(
                  Icons.shopping_basket,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Aboutas()));
              },
              child: ListTile(
                title: Text('About Us'),
                leading: Icon(
                  Icons.help,
                  color: Colors.lightGreen,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacement((context),
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
              child: ListTile(
                title: Text('Login Out'),
                leading: Icon(
                  Icons.lock_open,
                  color: Colors.lightGreen,
                ),
              ),
            ),
          ],
        ),
      ),
      body: _Drawernavigaor(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.green.withOpacity(0.75),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SelectCategory(id: widget.logeduser.id)));
        },
      ),
    );
  }
}

Future stop() async {
  await Future.delayed(Duration(seconds: 2));
}

class ViewallItem extends StatefulWidget {
  user.Usr logeduser;
  Detaileduser userdetails;
  String url = 'http://34.87.38.40/purchaseItem/viewAll';

  // ViewallItem ref1 = ViewallItem();

  //  ViewallItem(){
    
  // }

  @override
  _ViewallItemState createState() => _ViewallItemState();
}

class _ViewallItemState extends State<ViewallItem> {
  Future<ListTile> llist;
  BuildContext _context;

  List<product> tmplist;
  bool isloading = true; //set this to false to load list

  Future loadproduct() async {
    //http.Response response =

    try {
      await http
          .get(
        'http://34.87.38.40/purchaseItem/viewAll',
      )
          .then((response) {
        //print(response.body);

        print(response.statusCode);
        print(response.body);
        if (response.statusCode == 200) {
          String content = response.body;
          List prdct = json.decode(content);
          if (content == '[]') {
            NoItempopup(this.context);
          } else {
            setState(() {
              tmplist = prdct.map((json) => product.fromjson(json)).toList();
              isloading = false;
            });
            print(tmplist[0].type);
          }
        } else if (response.statusCode >= 400 && response.statusCode < 500) {
          print('error');
        } else if (response.statusCode >= 500) {
          ServerErrorPopup(this.context);
        }
      });
    } catch (e) {
      print(e);
      NetworkErrorPopup(context);
    }
  }

  Future filterproduct() async {
    Map arr = {
      "district": district.text,
      "city": city.text ,
      "type": type.text,
      "variety": variety.text
    };
    print(arr);

    var filterarr = jsonEncode(arr);

    try {
      await http
          .post('http://34.87.38.40/purchaseItem/filter',
              headers: {"Content-Type": "application/json"}, body: filterarr)
          .then((response) {
        //print(response.body);

        print(response.statusCode);
        print(response.body);
        if (response.statusCode == 200) {
          String content = response.body;
          List prdct = json.decode(content);
          if (content == '[]') {
            NoItempopup(this.context);
          } else {
            setState(() {
              tmplist = prdct.map((json) => product.fromjson(json)).toList();
              isloading = false;
            });
            print(tmplist[0].type);
          }
        } else if (response.statusCode >= 400 && response.statusCode < 500) {
          print('error');
        } else if (response.statusCode >= 500) {
          ServerErrorPopup(this.context);
        }
      });
    } catch (e) {
      print(e);
      NetworkErrorPopup(context);
    }
  }

  TextEditingController district = TextEditingController(text:'null');
  TextEditingController city = TextEditingController(text: 'null');
  TextEditingController type = TextEditingController(text: 'null');
  TextEditingController variety = TextEditingController(text: 'null');

  @override
  void initState() {
    loadproduct();
   // filterproduct();
    //isloading=false;
  }

  Map arr = Map();
  var districts = ['kurunegala', 'Kandy', 'gampaha','Anuradhapuraya', 'polonnruwa'];

  var refreshkey = GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Container(
            child: Column(
          children: <Widget>[
            new Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 0),
                child: new Text(
                  'Filter',
                  style: TextStyle(fontWeight: FontWeight.bold),
                )),
            // HorizontalList(),
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: DropdownButton<String>(
                          items: districts.map((String dis) {
                            return DropdownMenuItem<String>(
                              value: dis,
                              child: Text(
                                dis,
                                style: TextStyle(fontSize: 12),
                              ),
                            );
                          }).toList(),
                          onChanged: (val) {
                            setState(() {
                              district.text=val;
                            });
                            
                          },
                          hint: Row(
                            children: <Widget>[
                              Text(district.text),
                            ],
                          ),
                        )
                        // TextFormField(
                        //   decoration: InputDecoration(
                        //       hintText: 'District',
                        //       hintStyle: TextStyle(fontSize: 12)),
                        // ),
                        ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: TextFormField(
                        controller: city,
                        //style: TextStyle(fontSize: 8),
                        decoration: InputDecoration(
                          
                            labelText: 'City',
                            hintStyle: TextStyle(fontSize: 12)),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: TextFormField(
                        controller: type,
                        decoration: InputDecoration(
                            labelText: 'Type',
                            hintStyle: TextStyle(fontSize: 12)),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: TextFormField(
                        controller: variety,
                        decoration: InputDecoration(
                          
                          labelText: 'variety',
                            //hintText: 'Variety',
                            hintStyle: TextStyle(fontSize: 12)),
                      ),
                    ),
                  ),
                  // // Expanded(child: FlatButton(child: Text('Filter'),onPressed: (){},),)
                ],
              ),
            ),
            new Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 25.0, 0, 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text('Recent List',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    FlatButton(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.filter_list,
                            color: Colors.white,
                          ),
                          Text(
                            'Filter',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                      onPressed: () async {
                        // await  Filterdialog(context,arr).then((onValue){
                        //   print(onValue);
                        //   setState(() {
                        //     arr = onValue;
                        //   });
                        filterproduct();
                        setState(() {
                          district.text=null;
                        city.text=null;
                        type.text=null;
                        variety.text=null;
                        });
                        
                        //   });
                        //   print(arr);
                        //await Filterdialog(context, arr);
                       // print(arr);
                        //Navigator.push(context, MaterialPageRoute(builder: (context)=>Drawer(child: Text('data'),)));
                      },
                      color: Colors.green.withOpacity(0.75),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                    )
                  ],
                )),
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        )),
        Expanded(
          child: isloading
              ? Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 3.0,
                  ),
                )
              : RefreshIndicator(
                  key: refreshkey,
                  child: ListView.builder(
                      itemCount: tmplist.length,
                      itemBuilder: (context, index) {
                        return Productlisttile(
                          context: context,
                          tile: tmplist[index],
                        );
                      }),
                  onRefresh: () async {
                    loadproduct();
                    await Future.delayed(Duration(seconds: 2));
                  },
                ),
          // isloading
          //     ? Center(
          //         child: CircularProgressIndicator(
          //         strokeWidth: 3.0,
          //       ))
          //     : ListView.builder(
          //         itemCount: tmplist.length,
          //         itemBuilder: (context, index) {
          //           return ListTile(
          //             leading: Container(child:  Text('data'),height: 100,width: 200,color: Colors.amber,),
          //             title: Text(tmplist[index].type),
          //             subtitle: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: <Widget>[
          //                 Text(
          //                   tmplist[index].quantity + "Kg",
          //                   style: TextStyle(fontWeight: FontWeight.bold),
          //                 ),
          //                 Text(tmplist[index].location)
          //               ],
          //             ),
          //             trailing: Text(tmplist[index].date),
          //             onTap: () {
          //               Navigator.push(
          //                   context,
          //                   MaterialPageRoute(
          //                       builder: (context) => viewitem(
          //                             cproduct: tmplist[index],canrequest: false,
          //                           )));
          //             },
          //             isThreeLine: true,
          //           );
          //         },
          //       ),
        ),
      ]),
    );
  }
}
