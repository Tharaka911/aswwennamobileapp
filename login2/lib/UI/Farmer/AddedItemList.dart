import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:login2/UI/Commenclass/Products.dart';
import 'package:flutter/material.dart';
import 'package:login2/userclass.dart';
import 'package:login2/UI/Commenclass/notificationstruct.dart';
import 'package:login2/UI/Farmer/Farmer.dart';
import 'package:login2/Method/buyermethod.dart';

class AddedItem extends StatefulWidget {
  int userid;

  AddedItem(this.userid);

  @override
  _AddedItemState createState() => _AddedItemState();
}

class _AddedItemState extends State<AddedItem> {
  BuildContext _context;
  List<product> gridview;
  bool isloading = true;

  Future loadproduct() async {
    var url = 'http://34.87.38.40/purchaseItem/' +
        widget.userid.toString() +
        '/requests';

    await http.get(url).then((response) {
      print(response.body);
      print(response.statusCode);
      if (response.statusCode == 200) {
        if(response.body=='[]'){
            NoItempopup(context);
            setState(() {
             
            });
        }else{
        String content = response.body;
        List prdct = json.decode(content);
        setState(() {
          gridview = prdct.map((json) => product.fromjson(json)).toList();
          isloading = false;
        });
        print(gridview);
        }
      }
      else if(response.statusCode>=400&&response.statusCode<500){
        print('error');
      }
      else if(response.statusCode>=500){
        ServerErrorPopup(context);
      }
    });
  }

  @override
  void initState() {
    loadproduct();
    //isloading=false;
  }

  @override
  Widget build(BuildContext context) {
    //loadproduct();
    return Scaffold(
      appBar: AppBar(
        title: Text('My Added Items'),
        backgroundColor: Colors.lightGreen,
      ),

      //body:Center(child: Text('Buyer'),),
      body: Container(
        child: isloading
            ? Center(
                child: CircularProgressIndicator(
                strokeWidth: 3.0,
              ))
            : ListView.builder(
                itemCount: gridview.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Icon(Icons.add_a_photo),
                    title: Text(gridview[index].type),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          gridview[index].quantity.toString() + "Kg",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(gridview[index].district)
                      ],
                    ),
                    trailing: Text(gridview[index].date),
                    onTap: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => viewitem(
                      //               cproduct: gridview[index],
                      //             )));
                    },
                    isThreeLine: true,
                  );
                },
              ),
      ),
    );
  }
}

// class product  {
//   int purchase_item_id;
//   String type;
//   String quantity;
//   String location;
//   String exp_date;
//   String date;
//   BuildContext route;

//   product({
//     this.purchase_item_id,
//     this.type,
//     this.quantity,
//     this.location,
//     this.date,
//     this.exp_date,
//   });

//   factory product.fromjson(Map<String, dynamic> json) {
//     return product(
//       type: json['type'],
//       quantity: json['quantity'],
//       date: json['date'],
//       location: json['location'],
//       exp_date: json['exp_date'],
//     );
//   }
// }
