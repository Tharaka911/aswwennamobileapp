import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:login2/imagepickerClass.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:login2/userclass.dart' as user;



class FarmerProfile extends StatefulWidget{

user.Usr logeduser;

FarmerProfile({this.logeduser});

  @override
    _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<FarmerProfile>{

ImagePic ipic=new ImagePic();

File _image;
final String url='https://www.cropscience.bayer.com/-/media/bcs-inter/ws_globalportal/crop-science/smallholder-farming/smallholder-farming-in-india.jpg';

Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      print('Image path $_image');
    });
  }

  Future uploadpic() async {
    var postBody= {
        'filename':ipic.filename,  
        "fileDownloadUri": ipic.fileDownloadUri,  
        "fileType":ipic.fileType,
        "size":ipic.size           
        //'image': imageFile != null ? base64Encode(imageFile.readAsBytesSync()) : '',
    };

    final response = await http.post(
      Uri.encodeFull(url),
      headers: {
        'Content-Type' : 'application/json',
      },
      body: json.encode(postBody),
    );

    final responseJson = json.decode(response.body);

    print(responseJson);
  }

  
  @override
  Widget build(BuildContext context) {

        return Scaffold(
          appBar: new AppBar(     
            backgroundColor: Colors.green.withOpacity(0.75),
            title: Text('Profile'),
            actions: <Widget>[
              new IconButton(icon: Icon(Icons.home,color: Colors.white,),onPressed: (){
                // Navigator.push(context,
                // MaterialPageRoute(builder: (context)=> FarmerInfo()));
              },),
            ],
          ),
         
          body: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/3,
                decoration: BoxDecoration(
                  color: Colors.green.withOpacity(0.75),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0)
                  )
                ),
                child: Column(
                  children: <Widget>[
                 
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            radius: 70,
                            backgroundColor: Colors.yellow,
                            child: ClipOval(
                              child: SizedBox(
                                width: 120,
                                height: 120,
                                child: (_image!=null)?Image.file(_image,fit: BoxFit.fill)
                                  :Image.network('https://www.cropscience.bayer.com/-/media/bcs-inter/ws_globalportal/crop-science/smallholder-farming/smallholder-farming-in-india.jpg',
                                  fit: BoxFit.fill,),
                                
                              ),
                            ),
                          )
                         
                        ),
                        Padding(
                          
                          padding: EdgeInsets.only(top: 60),
                          child: IconButton(
                            icon: Icon(
                              Icons.add_a_photo,
                          size: 30.0,
                        ), 
                        onPressed: () {
                           // getImage();
                        },
                      ),
                    )
                  ],
                ),
                
                Text('Isuru Madampitge',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,                      
                    ),
                ),
                Text('id:1234567',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                    ),
                )
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10,left: 10),
                  child: Container(
                    width: 400,
                    height: 350,
                    //color: Colors.green,
                    decoration: BoxDecoration(
                        color: Colors.white24,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10))
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 2),
                              child: Icon(Icons.person),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('Name:',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold
                                        ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('isuru sampath:',
                                  style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold
                                        ),
                              ),
                            ),
                          ],
                        ),
                        
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 2),
                              child: Icon(Icons.email),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('Email:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold

                                        ),
                                    ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('madampitige90@gmail.com',
                                  style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold
                                        ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 2),
                              child: Icon(Icons.phone),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('Mobile Number:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold
                                        ),
                                    ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30,left: 10),
                              child: Text('0777725422',
                                  style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold
                                        ),
                              ),
                            ),
                          ],
                        ),   
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: RaisedButton(
                                color: Colors.green,
                                onPressed: (){
                                  Navigator.of(context).pop();
                                },
                                elevation: 4.0,
                                splashColor: Colors.blueGrey,
                                child: Text('Cancel',
                                  style: TextStyle(color: Colors.white,fontSize: 16),
                                ),
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.only(left: 40,top: 20),
                              child: RaisedButton(
                                color: Colors.green,
                                onPressed: (){
                                  uploadpic();
                                },
                                elevation: 4.0,
                                splashColor: Colors.blueGrey,
                                child: Text('Submit',
                                  style: TextStyle(color: Colors.white,fontSize: 16),
                                ),
                              ),
                            )
                          ],
                        )  
                          
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
